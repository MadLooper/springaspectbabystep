package Aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class Komunikacja {

    @After("execution(* API.IKucharz.pobierzSkladnikiZMagazynu(..))")
    public void poinformujOPobraniuSkladnikowZmagazynu(){
        System.out.println("Pobrano skladniki z magazynu");
    }


    @Around("execution(* API.IKucharz.wydajPizze(..))")
public void wydaniePizzy(ProceedingJoinPoint joinPoint){
        //joinPoint - umozliwia sterowanie przed i po wykonaniem metody
        try {
            System.out.println("Komunikujemy, ze pizza jest gotowa");
            joinPoint.proceed();
            System.out.println("Komunikujemy, ze wydano pizze");

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

    }
}
