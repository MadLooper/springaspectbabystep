package Aspect;

import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class KontrolaMagazynu {

    @Pointcut("execution(* API.IKucharz.pobierzSkladnikiZMagazynu(..))")
    public void pointCut(){

    }
    @Before("execution(* API.IKucharz.pobierzSkladnikiZMagazynu(..))")
    public void sprawdzSkladnikiMagazynu(){
        System.out.println("Sprawdzam stan magazynu");
    }

    @AfterThrowing("execution(* API.IKucharz.pobierzSkladnikiZMagazynu(..))")
public void sygnalizujBrakiProduktowWMagazynie(){
        System.out.println("Nie ma czegos w magazynie!!");
    }
}
