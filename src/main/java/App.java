import API.IKucharz;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import Config.Config;

public class App {
    public static void main(String[] args) {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        IKucharz k = context.getBean(IKucharz.class);
        k.pobierzSkladnikiZMagazynu();

        k.przygotujPizze();
        k.wydajPizze();
    }
}
