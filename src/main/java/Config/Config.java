package Config;

import API.IKucharz;
import API.Kucharz;
import Aspect.Komunikacja;
import Aspect.KontrolaMagazynu;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class Config {

    @Bean
    public IKucharz kucharz(){
        return new Kucharz();

    }
    @Bean
    public Komunikacja komunikacja (){
        return new Komunikacja();

    }
    @Bean
    public KontrolaMagazynu kontrolaMagazynu(){
        return new KontrolaMagazynu();
    }
}
